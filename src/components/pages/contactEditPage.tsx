import { SyntheticEvent } from 'react';
import { Form, useParams } from 'react-router-dom';
import { useContactEdit } from '@hooks/contacts/useContactEdit';
import { Card } from '@mui/material';
import { IPerson } from '@lib/models/person';

export function ContactEditPage() {
  const { id } = useParams();
  const { contact, update } = useContactEdit(id);

  function handleSubmit(event: SyntheticEvent<HTMLFormElement>) {
    event.preventDefault();
    const form = event.currentTarget;

    const person: IPerson = {
      id: form.identity.value,
      firstName: form.firstName.value,
      lastName: form.lastName.value,
      email: form.email.value
    };

    update(person);
  }

  return (
    <Card>
      <Form onSubmit={handleSubmit}>
        <h1>Contact Edit Page</h1>

        <input type="hidden" name="identity" defaultValue={contact?.id} />
        <p>
          <strong>First Name:</strong>
          <input type="text" name="firstName" defaultValue={contact?.firstName} />
        </p>
        <p>
          <strong>Last Name:</strong> {contact?.lastName}
          <input type="text" name="lastName" defaultValue={contact?.lastName} />
        </p>
        <p>
          <strong>Email:</strong> {contact?.email}
          <input type="text" name="email" defaultValue={contact?.email} />
        </p>

        <input type="submit" value="Update" />
      </Form>
    </Card>
  );
}
