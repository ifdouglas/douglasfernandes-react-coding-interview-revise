import { contactsClient } from '@lib/clients/contacts';
import { IPerson } from '@lib/models/person';
import { useCallback, useEffect, useState } from 'react';

export function useContactEdit(id: string) {
  const [data, setData] = useState<IPerson | null>(null);

  useEffect(() => {
    (async () => {
      const data = await contactsClient.getContactById(id);
      setData(data);
    })();
  }, []);

  return {
    contact: data as IPerson,
    update: useCallback((updated: IPerson) => {
      console.log('Calling update (contact)', updated);
      contactsClient.updateContact(id, updated);
    }, [])
  };
}
